package com.kata.vendingmachine.response.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;

public class VendingMachineResponseTest {

	@Test
	public void testGetItem() {
		VendingMachineResponse response = new VendingMachineResponse(
				VendingMachineItem.PRODUCT_A,
				Collections.<VendingMachineMoney> emptyList());
		Assert.assertEquals(VendingMachineItem.PRODUCT_A, response.getItem());
	}

	@Test
	public void testGetCoinsToReturn() {
		List<VendingMachineMoney> returnCoins = new ArrayList<VendingMachineMoney>();
		returnCoins.add(VendingMachineMoney.NICKEL);
		returnCoins.add(VendingMachineMoney.QUARTER);
		VendingMachineResponse response = new VendingMachineResponse(
				VendingMachineItem.PRODUCT_B, returnCoins);
		Assert.assertEquals(returnCoins, response.getCoinsToReturn());
	}

	@Test
	public void testGetReturnAmount() {
		List<VendingMachineMoney> returnCoins = new ArrayList<VendingMachineMoney>();
		returnCoins.add(VendingMachineMoney.DIME);
		returnCoins.add(VendingMachineMoney.DOLLLAR);
		VendingMachineResponse response = new VendingMachineResponse(
				VendingMachineItem.PRODUCT_A, returnCoins);
		Assert.assertEquals(110, response.getReturnAmount());
	}
}
