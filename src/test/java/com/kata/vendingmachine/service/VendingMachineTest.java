package com.kata.vendingmachine.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kata.vendingmachine.exceptions.InsertMoreCoinsException;
import com.kata.vendingmachine.exceptions.ItemNotAvailableException;
import com.kata.vendingmachine.exceptions.ChangeNotAvailableException;
import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;
import com.kata.vendingmachine.response.dto.VendingMachineResponse;
import com.kata.vendingmachine.service.VendingMachine;

public class VendingMachineTest {

	private VendingMachine vendingMachine;

	private VendingMachineTestUtility testUtility;

	@Before
	public void setUp() {
		vendingMachine = new VendingMachine();
		testUtility = new VendingMachineTestUtility(vendingMachine);
	}

	@Test(expected = IllegalArgumentException.class)
	public void givenACentWhenInsertedIntoMachineThenErrorIsThrown() {
		testUtility.insertCoins(1L);
	}

	@Test
	public void givenNoMoneyInMachineWhenNickelIsInsertedThenTotalEquals5() {
		testUtility.insertCoins(5L);
		assertEquals(5, vendingMachine.getCurrentlyInsertedAmount());
	}

	@Test
	public void givenNickelInMachineWhenDimeIsInsertedThenTotalEquals15() {
		// Set vending machine total to 5 cents and insert dime
		testUtility.insertCoins(5L, 10L);
		assertEquals(15, vendingMachine.getCurrentlyInsertedAmount());
	}

	@Test
	public void givenNickelAndDimeInMachineWhenTwoQuartersAreInsertedThenTotalEquals65() {
		// Set vending machine total to 15 cents and insert 2 quarters
		testUtility.insertCoins(5L, 10L, 25L, 25L);
		assertEquals(65, vendingMachine.getCurrentlyInsertedAmount());
	}

	@Test
	public void givenANickelAndQuarterInMachineWhenReturnCoinIsSelectedThenNickelAndQuarterAreReturned() {
		// set vending machine total to 30 cents
		testUtility.insertCoins(5L, 25L);

		List<VendingMachineMoney> expectedCollection = new ArrayList<VendingMachineMoney>();
		expectedCollection.add(VendingMachineMoney.NICKEL);
		expectedCollection.add(VendingMachineMoney.QUARTER);
		// Select coin return
		Collection<VendingMachineMoney> actualCollection = vendingMachine.returnInsertedCoins();
		Assert.assertEquals(expectedCollection, actualCollection);
	}

	@Test
	public void givenNewVendingMachineWhenServiceIsDoneThenItemIsAvailable() {
		initializeQuantityMaps();
		assertEquals(10, vendingMachine.getAvailableQuantityForChange(VendingMachineMoney.DIME));
		assertEquals(5, vendingMachine.getAvailableQuantityForItem(VendingMachineItem.PRODUCT_A));
	}

	@Test
	public void givenADollarInMachineWhenVendBIsSelectedThenBIsReturned()
			throws InsertMoreCoinsException, ItemNotAvailableException, ChangeNotAvailableException {
		setUpVendingMachineForVendingProduct(1, 0, 25L, 25L, 25L, 25L);

		VendingMachineResponse response = vendingMachine.vend(VendingMachineItem.PRODUCT_B);
		assertEquals(VendingMachineItem.PRODUCT_B, response.getItem());
	}

	@Test
	public void givenADollarInMachineWhenVendAIsSelectedThenAAnd35CentsAreReturned()
			throws InsertMoreCoinsException, ItemNotAvailableException, ChangeNotAvailableException {
		// set vending machine total to 100 cents
		setUpVendingMachineForVendingProduct(5, 5, 100L);

		// Select item A
		List<VendingMachineMoney> expectedReturnCoins = new ArrayList<VendingMachineMoney>();
		expectedReturnCoins.add(VendingMachineMoney.QUARTER);
		expectedReturnCoins.add(VendingMachineMoney.DIME);
		VendingMachineResponse expected = new VendingMachineResponse(VendingMachineItem.PRODUCT_A, expectedReturnCoins);

		VendingMachineResponse actual = vendingMachine .vend(VendingMachineItem.PRODUCT_A);
		assertEquals(expected, actual);
	}

	@Test(expected = InsertMoreCoinsException.class)
	public void givenADimeInMachineWhenVendAIsSelectedThenErrorIsThrown()
			throws InsertMoreCoinsException, ItemNotAvailableException, ChangeNotAvailableException {
		setUpVendingMachineForVendingProduct(1, 0, 10L);
		vendingMachine.vend(VendingMachineItem.PRODUCT_A);
	}

	@Test(expected = ItemNotAvailableException.class)
	public void givenADollarInMachineAndItemBIsUnavailableWhenVendBIsSelectedThenErrorIsThrown()
			throws InsertMoreCoinsException, ItemNotAvailableException,ChangeNotAvailableException {
		testUtility.insertCoins(100L);
		vendingMachine.vend(VendingMachineItem.PRODUCT_B);
	}

	@Test(expected = ChangeNotAvailableException.class)
	public void givenADollarInMachineAndChangeIsUnavailableWhenVendAIsSelectedThenErrorIsThrown()
			throws InsertMoreCoinsException, ItemNotAvailableException, ChangeNotAvailableException {
		setUpVendingMachineForVendingProduct(5, 0, 100L);
		vendingMachine.vend(VendingMachineItem.PRODUCT_A);
	}

	@Test
	public void givenADollarInMachineWhenVendBIsSelectedThenBIsReturnedAndQuantityOfBIsReduced()
			throws InsertMoreCoinsException, ItemNotAvailableException, ChangeNotAvailableException {
		setUpVendingMachineForVendingProduct(20, 0, 100L);
		vendingMachine.vend(VendingMachineItem.PRODUCT_B);
		assertEquals(19, vendingMachine.getAvailableQuantityForItem(VendingMachineItem.PRODUCT_B));
	}

	@Test
	public void givenFourQuartersInMachineWhenVendBIsSelectedThenBIsReturnedAndQuantityOfQuartersIsIncreased()
			throws InsertMoreCoinsException, ItemNotAvailableException, ChangeNotAvailableException {
		setUpVendingMachineForVendingProduct(5, 5, 25L, 25L, 25L, 25L);
		vendingMachine.vend(VendingMachineItem.PRODUCT_B);
		assertEquals(9, vendingMachine.getAvailableQuantityForChange(VendingMachineMoney.QUARTER));
	}

	private void setUpVendingMachineForVendingProduct(int itemQuantity, int changeQuantity, Long...coinsToInsert) {
		testUtility.insertCoins(coinsToInsert);
		testUtility.setAvaliableItemsWithQuantity(itemQuantity);
		testUtility.setAvaliableChangesWithQuantity(changeQuantity);
	}

	private void initializeQuantityMaps() {
		Map<VendingMachineItem, Integer> itemQuantities = new HashMap<VendingMachineItem, Integer>();
		itemQuantities.put(VendingMachineItem.PRODUCT_A, 5);
		Map<VendingMachineMoney, Integer> changeQuantities = new HashMap<VendingMachineMoney, Integer>();
		changeQuantities.put(VendingMachineMoney.DIME, 10);
		vendingMachine.setAvaliableChangesAndItems(itemQuantities,
				changeQuantities);
	}
}
