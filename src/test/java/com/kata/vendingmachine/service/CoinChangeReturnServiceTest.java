package com.kata.vendingmachine.service;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.kata.vendingmachine.exceptions.InsertMoreCoinsException;
import com.kata.vendingmachine.exceptions.ChangeNotAvailableException;
import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;

public class CoinChangeReturnServiceTest {

	private VendingMachine machine;

	private VendingMachineTestUtility testUtility;

	@Before
	public void setUp() {
		machine = new VendingMachine();
		testUtility = new VendingMachineTestUtility(machine);
	}

	@Test
	public void givenOwedAmountIsZeroThenCoinsToReturnIsEmpty()
			throws InsertMoreCoinsException, ChangeNotAvailableException {
		testUtility.insertCoins(100L);
		Assert.assertTrue(new CoinChangeReturnService(machine)
				.getCoinsToReturn(VendingMachineItem.PRODUCT_B).isEmpty());
	}

	@Test(expected = InsertMoreCoinsException.class)
	public void givenOwedAmountIsNegativeThenCoinsToReturnIsEmpty()
			throws InsertMoreCoinsException, ChangeNotAvailableException {
		testUtility.insertCoins(10L, 5L);
		Assert.assertTrue(new CoinChangeReturnService(machine)
				.getCoinsToReturn(VendingMachineItem.PRODUCT_B).isEmpty());
	}

	@Test
	public void givenOwedAmountIsPositiveAndChangeIsAvailableWhenVendRequestedThenCoinsAreReturned()
			throws InsertMoreCoinsException, ChangeNotAvailableException {
		testUtility.insertCoins(100L);
		testUtility.setAvaliableChangesAndItemsWithQuantity(10);

		Collection<VendingMachineMoney> coinsToReturnResult = new CoinChangeReturnService(
				machine).getCoinsToReturn(VendingMachineItem.PRODUCT_A);
		Assert.assertEquals(2, coinsToReturnResult.size());
		Assert.assertTrue(coinsToReturnResult.contains(VendingMachineMoney.QUARTER));
		Assert.assertTrue(coinsToReturnResult.contains(VendingMachineMoney.DIME));
	}

	@Test(expected = ChangeNotAvailableException.class)
	public void givenOwedAmountIsPositiveAndChangeIsNotAvailableWhenVendRequestedThenCoinsAreNotReturned()
			throws InsertMoreCoinsException, ChangeNotAvailableException {
		testUtility.insertCoins(100L);
		new CoinChangeReturnService(machine).getCoinsToReturn(VendingMachineItem.PRODUCT_A);
	}
}
