package com.kata.vendingmachine.service;

import java.util.HashMap;
import java.util.Map;

import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;

public class VendingMachineTestUtility {

	private final VendingMachine machine;

	public VendingMachineTestUtility(VendingMachine machine) {
		this.machine = machine;
	}

	public void insertCoins(Long... amounts) {
		for (long amount : amounts) {
			machine.insert(amount);
		}
	}

	public void setAvaliableChangesAndItemsWithQuantity(int quantity) {
		Map<VendingMachineItem, Integer> itemQuantities = new HashMap<VendingMachineItem, Integer>();
		itemQuantities.put(VendingMachineItem.PRODUCT_A, quantity);
		itemQuantities.put(VendingMachineItem.PRODUCT_B, quantity);
		itemQuantities.put(VendingMachineItem.PRODUCT_C, quantity);
		Map<VendingMachineMoney, Integer> changeQuantities = new HashMap<VendingMachineMoney, Integer>();
		changeQuantities.put(VendingMachineMoney.NICKEL, quantity);
		changeQuantities.put(VendingMachineMoney.DIME, quantity);
		changeQuantities.put(VendingMachineMoney.QUARTER, quantity);
		machine.setAvaliableChangesAndItems(itemQuantities, changeQuantities);
	}

	public void setAvaliableChangesWithQuantity(int quantity) {
		Map<VendingMachineMoney, Integer> changeQuantities = new HashMap<VendingMachineMoney, Integer>();
		changeQuantities.put(VendingMachineMoney.NICKEL, quantity);
		changeQuantities.put(VendingMachineMoney.DIME, quantity);
		changeQuantities.put(VendingMachineMoney.QUARTER, quantity);
		machine.setAvaliableChangesAndItems(null, changeQuantities);
	}

	public void setAvaliableItemsWithQuantity(int quantity) {
		Map<VendingMachineItem, Integer> itemQuantities = new HashMap<VendingMachineItem, Integer>();
		itemQuantities.put(VendingMachineItem.PRODUCT_A, quantity);
		itemQuantities.put(VendingMachineItem.PRODUCT_B, quantity);
		itemQuantities.put(VendingMachineItem.PRODUCT_C, quantity);
		machine.setAvaliableChangesAndItems(itemQuantities, null);
	}
}
