package com.kata.vendingmachine.model;

/**
 * Representation of items that the vending machine offers with their respective
 * prices
 */
public enum VendingMachineItem {

	PRODUCT_A(65), PRODUCT_B(100), PRODUCT_C(150);
	private final long price;

	private VendingMachineItem(long price) {
		this.price = price;
	}

	/**
	 * @return price for this item
	 */
	public long getPrice() {
		return this.price;
	}

	public String getSelector() {
		return this.name();
	}
}
