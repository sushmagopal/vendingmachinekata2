package com.kata.vendingmachine.model;

/**
 * Representation of money which this vending machine accepts
 */
public enum VendingMachineMoney {

	DOLLLAR(100), QUARTER(25), DIME(10), NICKEL(5);

	private final long value;

	private VendingMachineMoney(long value) {
		this.value = value;
	}

	public long getValue() {
		return this.value;
	}
}
