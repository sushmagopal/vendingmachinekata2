package com.kata.vendingmachine.exceptions;

public class InsertMoreCoinsException extends Exception{
	private static final long serialVersionUID = 6429798343235528611L;

	public InsertMoreCoinsException() {
		super("Please insert more coins!");
	}
}
