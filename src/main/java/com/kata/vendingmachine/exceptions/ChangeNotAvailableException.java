package com.kata.vendingmachine.exceptions;

public class ChangeNotAvailableException extends Exception {

	private static final long serialVersionUID = -4564728881039044417L;

	public ChangeNotAvailableException() {
		super("Please provide exact change!");
	}

}
