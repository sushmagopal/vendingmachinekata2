package com.kata.vendingmachine.exceptions;

public class ItemNotAvailableException extends Exception {

	private static final long serialVersionUID = -254508923191738576L;

	public ItemNotAvailableException() {
		super("Selected item not available!");
	}

}
