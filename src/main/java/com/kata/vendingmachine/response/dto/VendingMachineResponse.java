package com.kata.vendingmachine.response.dto;

import java.util.Collection;

import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;

/**
 * DTO representing the response given by the vending machine after money is
 * given and item is selected
 */
public class VendingMachineResponse {
	private final VendingMachineItem item;

	private final Collection<VendingMachineMoney> coinsToReturn;

	public VendingMachineResponse(VendingMachineItem item,
			Collection<VendingMachineMoney> coinsToReturn) {
		this.item = item;
		this.coinsToReturn = coinsToReturn;
	}

	/**
	 * @return item to return to user
	 */
	public VendingMachineItem getItem() {
		return item;
	}

	/**
	 * @return coins to return to user
	 */
	public Collection<VendingMachineMoney> getCoinsToReturn() {
		return coinsToReturn;
	}

	public long getReturnAmount() {
		long returnAmount = 0;
		for (VendingMachineMoney money : coinsToReturn) {
			returnAmount += money.getValue();
		}
		return returnAmount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result
				+ ((coinsToReturn == null) ? 0 : coinsToReturn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendingMachineResponse other = (VendingMachineResponse) obj;
		if (item != other.item)
			return false;
		if (coinsToReturn == null) {
			if (other.coinsToReturn != null)
				return false;
		} else if (!coinsToReturn.equals(other.coinsToReturn))
			return false;
		return true;
	}
}
