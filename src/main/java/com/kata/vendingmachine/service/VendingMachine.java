package com.kata.vendingmachine.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kata.vendingmachine.exceptions.InsertMoreCoinsException;
import com.kata.vendingmachine.exceptions.ItemNotAvailableException;
import com.kata.vendingmachine.exceptions.ChangeNotAvailableException;
import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;
import com.kata.vendingmachine.response.dto.VendingMachineResponse;

public class VendingMachine {

	private List<VendingMachineMoney> insertedMoney;

	private Map<VendingMachineItem, Integer> itemQuantities;

	private Map<VendingMachineMoney, Integer> changeQuantities;

	public VendingMachine() {
		insertedMoney = new ArrayList<VendingMachineMoney>();
		initializeChangeQuantityMap();
		initializeItemQuantityMap();
	}

	/**
	 * @return total amount the customer has inserted into the vending machine
	 */
	public long getCurrentlyInsertedAmount() {
		long currentInsertedAmount = 0;
		for (VendingMachineMoney money : insertedMoney) {
			currentInsertedAmount += money.getValue();
		}
		return currentInsertedAmount;
	}

	/**
	 * @param insertedAmount
	 * @throws IllegalArgumentException
	 *             when the coin or dollar bill inserted is not accepted by
	 *             vending machine
	 */
	public void insert(long insertedAmount) {
		for (VendingMachineMoney money : VendingMachineMoney.values()) {
			if (money.getValue() == insertedAmount) {
				insertedMoney.add(money);
				return;
			}
		}
		throw new IllegalArgumentException(
				"Please insert only dimes, nickels, quarters and dollar bills");
	}

	/**
	 * @return all coins inserted by the customers
	 */
	public Collection<VendingMachineMoney> returnInsertedCoins() {
		return this.insertedMoney;
	}

	/**
	 * @param item
	 *            item selection made by the customer
	 * @return the response consisting of item and coin change to be provided
	 * @throws InsertMoreCoinsException
	 * @throws ChangeNotAvailableException
	 * @throws ItemNotAvailableException
	 */
	public VendingMachineResponse vend(VendingMachineItem item)
			throws InsertMoreCoinsException, ItemNotAvailableException,
			ChangeNotAvailableException {
		// If item is unavailable
		if (itemQuantities.get(item) == 0) {
			throw new ItemNotAvailableException();
		}
		VendingMachineResponse response = new VendingMachineResponse(item,
				new CoinChangeReturnService(this).getCoinsToReturn(item));

		decrementQuantityForItem(item);
		incrementQuantityForCoinFromCollection(insertedMoney);
		decrementQuantityForCoinFromCollection(response.getCoinsToReturn());
		resetInsertedMoney();
		return response;
	}

	/**
	 * This is for the service person to open the vending machine and set
	 * available changes and items
	 *
	 * @param addItemQuantities
	 * @param addChangeQuantities
	 */
	public void setAvaliableChangesAndItems(
			Map<VendingMachineItem, Integer> addItemQuantities,
			Map<VendingMachineMoney, Integer> addChangeQuantities) {
		if (addItemQuantities != null) {
			for (VendingMachineItem item : addItemQuantities.keySet()) {
				incrementQuantityForItem(item, addItemQuantities.get(item));
			}
		}
		if (addChangeQuantities != null) {
			for (VendingMachineMoney change : addChangeQuantities.keySet()) {
				incrementQuantityForCoin(change,
						addChangeQuantities.get(change));
			}
		}
	}

	private void initializeItemQuantityMap() {
		itemQuantities = new HashMap<VendingMachineItem, Integer>();
		for (VendingMachineItem item : VendingMachineItem.values()) {
			itemQuantities.put(item, 0);
		}
	}

	private void initializeChangeQuantityMap() {
		changeQuantities = new HashMap<VendingMachineMoney, Integer>();
		changeQuantities.put(VendingMachineMoney.NICKEL, 0);
		changeQuantities.put(VendingMachineMoney.DIME, 0);
		changeQuantities.put(VendingMachineMoney.QUARTER, 0);
	}

	private void resetInsertedMoney() {
		this.insertedMoney = new ArrayList<VendingMachineMoney>();
	}

	private void decrementQuantityForCoinFromCollection(
			Collection<VendingMachineMoney> returnCoins) {
		for (VendingMachineMoney change : returnCoins) {
			incrementQuantityForCoin(change, -1);
		}
	}

	private void incrementQuantityForCoinFromCollection(
			Collection<VendingMachineMoney> insertedMoney) {
		for (VendingMachineMoney change : insertedMoney) {
			incrementQuantityForCoin(change, 1);
		}
	}

	private void incrementQuantityForCoin(VendingMachineMoney coin, int quantity) {
		int value = quantity;
		if (changeQuantities.get(coin) != null) {
			value = value + changeQuantities.get(coin);
		}
		changeQuantities.put(coin, value);
	}

	private void decrementQuantityForItem(VendingMachineItem item) {
		itemQuantities.put(item, itemQuantities.get(item) - 1);
	}

	private void incrementQuantityForItem(VendingMachineItem item, int quantity) {
		int value = quantity;
		if (itemQuantities.get(item) != null) {
			value = value + itemQuantities.get(item);
		}
		itemQuantities.put(item, value);
	}

	int getAvailableQuantityForItem(VendingMachineItem item) {
		return itemQuantities.get(item);
	}

	int getAvailableQuantityForChange(VendingMachineMoney change) {
		return changeQuantities.get(change);
	}
}
