package com.kata.vendingmachine.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.kata.vendingmachine.exceptions.InsertMoreCoinsException;
import com.kata.vendingmachine.exceptions.ChangeNotAvailableException;
import com.kata.vendingmachine.model.VendingMachineItem;
import com.kata.vendingmachine.model.VendingMachineMoney;

public class CoinChangeReturnService {

	private final VendingMachine vendingMachine;

	public CoinChangeReturnService(VendingMachine vendingMachine) {
		this.vendingMachine = vendingMachine;
	}

	public Collection<VendingMachineMoney> getCoinsToReturn(
			VendingMachineItem selectedItem) throws InsertMoreCoinsException,
			ChangeNotAvailableException {

		long currentAmount = vendingMachine.getCurrentlyInsertedAmount();
		long itemPrice = selectedItem.getPrice();

		if (currentAmount < itemPrice) {
			throw new InsertMoreCoinsException();
		}

		return getCoinsToReturn(currentAmount - itemPrice);
	}

	private Collection<VendingMachineMoney> getCoinsToReturn(long owedAmount)
			throws ChangeNotAvailableException {

		List<VendingMachineMoney> coinsToReturn = new ArrayList<VendingMachineMoney>();
		while (owedAmount > 0) {
			long beforeCoinAdd = owedAmount;
			owedAmount = addCoinToReturnList(owedAmount, coinsToReturn);
			if (owedAmount == beforeCoinAdd) {
				throw new ChangeNotAvailableException();
			}
		}

		return coinsToReturn;
	}

	private long addCoinToReturnList(long owedAmount,
			Collection<VendingMachineMoney> coinsToReturn) {
		for (VendingMachineMoney coin : VendingMachineMoney.values()) {
			if (toAddCoinToList(owedAmount, coin)) {
				coinsToReturn.add(coin);
				return owedAmount - coin.getValue();
			}
		}
		return owedAmount;
	}

	private boolean toAddCoinToList(long owedAmount, VendingMachineMoney coin) {
		return owedAmount >= coin.getValue()
				&& vendingMachine.getAvailableQuantityForChange(coin) > 0;
	}

}
